<?php

namespace FarmersOnline\Timezonelist\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * The Timezonelist facade.
 *
 * @package FarmersOnline\Timezonelist\Facades
 * @author Jackie Do <anhvudo@gmail.com>
 * @author Robert Boerema <robert.boeremaa@farmersonline.eu>
 */
class Timezonelist extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'timezonelist';
    }
}
